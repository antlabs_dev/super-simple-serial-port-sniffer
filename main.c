#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <termios.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/serial.h> 

#define VERSION_HIGH 0
#define VERSION_LOW 5

typedef struct {
	enum { ASCII_MODE, HEX_MODE } mode;
	enum { WITH_TIME, WITHOUT_TIME } timestamp;
	unsigned short hex_bpl;
} CharMode;

typedef struct {
	int *port;
        char *endl;
        CharMode *mode;
} ReadThreadOptions;

static int open_port(const char *name, speed_t baudrate, struct termios *oldopts);
static void close_port(const int *fd, const char *name, const struct termios *oldopts);
static void help_dial(const char *argv0);
static void read_cycle(const int *port, const char *endl, const CharMode *mode);
static void set_exit_flag(int signal);
static char *ihtoa(char str);
static char *gettime(void);

static volatile int exit_flag=0;

static pthread_t read_thread;
static void *read_thread_start(void *args);

int main(int argc, char *argv[])
{	
	fprintf(stdout, "\033[36mSuper simple serial terminal version %u.%.2u\033[0m\n", VERSION_HIGH, VERSION_LOW);
	if(argc == 1)
	{
		help_dial(argv[0]);
		exit(EXIT_FAILURE);
	}
	char *portname=NULL;
	char endl=0;
	unsigned int i_baudrate=0;
	CharMode mode={ASCII_MODE, WITHOUT_TIME, 0};

	int opt;
	while((opt = getopt(argc, argv, "htx:n:p:s:")) != -1)
	{
		switch(opt)
		{
			case 'p': 
				portname = malloc(strlen(optarg)+4);
				if(portname == NULL)
				{
					fprintf(stderr, "Cannot allocate memory for port name: %s\n", strerror(errno));
					exit(EXIT_FAILURE);
				}
				else
				{
					strcpy(portname, optarg);
				}
				break;
			case 's':
				i_baudrate = strtoul(optarg, NULL, 10);
				break;
			case 'n':
				if(strcmp(optarg, "lf") == 0)
				{
					endl = '\n';
				}
				else if(strcmp(optarg, "cr") == 0)
				{
					endl = '\r';
				}
				else
				{
					fprintf(stderr, "Unknown newline symbol type %s\n", optarg);
					help_dial(argv[0]);
					exit(EXIT_FAILURE);
				}
				break;
			case 'x':
			       mode.mode = HEX_MODE;	
			       mode.hex_bpl = strtoul(optarg, NULL, 10);
				if(mode.hex_bpl == 0 || mode.hex_bpl > 256)
				{
					fprintf(stderr, "Invalid hex bytes per line number\n");
					exit(EXIT_FAILURE);
				}
			       break;
			case 't':
			       mode.timestamp = WITH_TIME;
			       break;
			case 'h':
			default:
				help_dial(argv[0]);
				exit(EXIT_SUCCESS);
		}
	}

	if(portname == NULL)
	{
		fprintf(stderr, "Invalid port name\n");
		exit(EXIT_FAILURE);
	}

	if(endl == 0)
	{
		fprintf(stderr, "Invalid new line character\n");
		exit(EXIT_FAILURE);
	}

	speed_t baudrate=0;
	
	switch(i_baudrate)
	{
		case 50: baudrate = B50; break;
		case 75: baudrate = B75; break;
		case 110: baudrate = B110; break;
		case 134: baudrate = B134; break;
		case 150: baudrate = B150; break;
		case 200: baudrate = B200; break;
		case 300: baudrate = B300; break;
		case 600: baudrate = B600; break;
		case 1200: baudrate = B1200; break;
		case 1800: baudrate = B1800; break;
		case 2400: baudrate = B2400; break;
		case 4800: baudrate = B4800; break;
		case 9600: baudrate = B9600; break;
		case 19200: baudrate = B19200; break;
		case 38400: baudrate = B38400; break;
		case 57600: baudrate = B57600; break;
		case 115200: baudrate = B115200; break;
		case 230400: baudrate = B230400; break;
		default:
		case 0:
			fprintf(stderr,"Invalid port speed %d\n",i_baudrate);
			exit(EXIT_FAILURE);
	}

	signal(SIGINT, set_exit_flag);
	signal(SIGTERM, set_exit_flag);
	
	struct termios oldopts;

	int port = open_port(portname, baudrate, &oldopts);

	ReadThreadOptions r_args;
	r_args.port = &port;
	r_args.endl = &endl;
	r_args.mode = &mode;
	
	pthread_create(&read_thread, NULL, read_thread_start, (void *)&r_args);
	//read_cycle(&port, &endl, &mode);
	void *status;
	pthread_join(read_thread, &status);

	fprintf(stdout, "Shutdown program...\n");

	close_port(&port, portname, &oldopts);
	
	exit(EXIT_SUCCESS);
}

static void *read_thread_start(void *args)
{
	ReadThreadOptions *a;
	a = (ReadThreadOptions *)args;
	read_cycle(a->port, a->endl, a->mode);
	pthread_exit(NULL);
}

static void read_cycle(const int *port, const char *endl, const CharMode *mode)
{
	char c=0;
	unsigned short i=0;
	while(exit_flag != 1)
	{
		if(read(*port, &c, sizeof(c)) != sizeof(c))
		{
			continue;	
		}
		switch(mode->mode)
		{
			case ASCII_MODE:
				if(c == *endl)
				{
					if(mode->timestamp == WITH_TIME)
					{
						fprintf(stdout, "\033[34m\t<%s>\033[0m\n", gettime());
					}
					else
					{
						fprintf(stdout, "\n");
					}
				}
				else
				{
					fprintf(stdout, "%c",c);
				}
				fflush(stdout);
				break;
			case HEX_MODE:
				if(i >= mode->hex_bpl)
				{
					if(mode->timestamp == WITH_TIME)
					{
						fprintf(stdout, "\033[34m\t<%s>\033[0m\n", gettime());
					}
					else
					{
						fprintf(stdout,"\n");
					}
					i=0;
				}
				else
				{
					fprintf(stdout, "%s ", ihtoa(c));
					i++;
				}
				fflush(stdout);
				break;
		}
	}
}


static int open_port(const char *name, speed_t baudrate, struct termios *oldopts)
{
	int port = open(name, O_RDONLY | O_NOCTTY | O_NDELAY);
	if(port < 0)
	{
		fprintf(stderr,"Cannot open port %s : %s\n", name, strerror(errno));	
		exit(EXIT_FAILURE);
	}
	else
	{   
	        fcntl(port, F_SETFL, 0/*FNDELAY*/); 
	}
        static struct termios opt;
        tcgetattr(port, oldopts);
        memset((void *)&opt, 0, sizeof(opt));
        cfsetispeed(&opt, baudrate);
        cfsetospeed(&opt, baudrate);
        opt.c_cflag |= (CLOCAL | CREAD);
        opt.c_cflag &= ~CSIZE;
        opt.c_cflag |= CS8;
        opt.c_cflag &= ~PARENB;
        opt.c_cflag &= ~CSTOPB;
        opt.c_cflag &= ~CSIZE;
        opt.c_cflag |= CS8;
        opt.c_lflag |= CRTSCTS | CS8 | CLOCAL | CREAD;
        opt.c_oflag &= ~OPOST;
        opt.c_iflag = IGNPAR;
        opt.c_cc[VMIN] = 1;
       	opt.c_cc[VTIME] = 0;
        tcflush(port, TCIFLUSH);
        tcsetattr(port, TCSANOW, &opt);

	struct serial_struct serial; 
	ioctl(port, TIOCGSERIAL, &serial);
	serial.flags |= ASYNC_LOW_LATENCY;
	ioctl(port, TIOCSSERIAL, &serial); 

	return port;
}

static void close_port(const int *fd, const char *name, const struct termios *oldopts)
{
	tcsetattr(*fd, TCSANOW, oldopts);
	if(close(*fd) < 0)
	{
		fprintf(stderr,"Cannot close port %s : %s\n", name, strerror(errno));
		exit(EXIT_FAILURE);
	}
}

static char *ihtoa(char str)
{
	unsigned char c;
	static char out[3];
	const char symbols[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	
	memset(out, 0, sizeof(out));

	c=(str>>4) & 0x0F;
	out[0]=symbols[c];
	c=(str & 0x0F);
	out[1]=symbols[c];
	out[2]='\0';

	return(out);
}

static char *gettime(void)
{
	time_t clock;
	struct tm *s_time;
	static char buff[64];

	memset(buff, 0, sizeof(buff));
	time(&clock);
	s_time = localtime(&clock);
	strftime(buff, sizeof(buff), "%b %d %H:%M:%S", s_time);
	return(buff);
}


static void help_dial(const char *argv0)
{
	fprintf(stdout, "\nSuper Simple Terminal by Oleg Antonyan\noleg.b.antonyan@gmail.com http://antlabs.ru\n\nUsage:\n\t%s OPTIONS ARGUMENTS\nAvailable OTIONS and ARGUMENTS:\n\t-p\tserial port device (/dev/ttyO2)\n\t-s\tserial port speed (57600)\n\t-n\tnew line character (lf, cr)\n\t-x\thex dispaly mode (hex numbers per line)\n\t-t\tadd timestamps at the end of line\nExample:\n\t%s -p /dev/ttyS0 -s 57600 -n lf -x 16 -t\n", argv0, argv0);
}

static void set_exit_flag(int signal)
{
	fprintf(stdout, "Signal %d caught\n", signal);
	exit_flag = 1;
	pthread_cancel(read_thread);
}
