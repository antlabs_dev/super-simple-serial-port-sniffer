CC=gcc
SOURCES=main.c
OUTNAME=ssterm
OPTIONS=-o $(OUTNAME) -Wall -pthread

all:
	cc $(SOURCES) $(OPTIONS)
	@echo Done! Run ./$(OUTNAME) now!

clean:
	rm -f $(OUTNAME)
	@echo Done!
